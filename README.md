**#About This Project**
 
This is backend server for the android app I built. The app name is Life+. 
This project provides all the REST API calls required for the Android app to run.
Technically this is not a full REST based project. This is something which I wrote myself to provide kinda REST based access.
This project uses Servlet to create REST based framework.

If you have any doubts, please contact ctsiva25@gmail.com



### #Setup ###

1. Just clone this repo.
2. Run pom.xml
3. Setup you local server (Tomcat)
4. You are good to go now.
5. Run this project.