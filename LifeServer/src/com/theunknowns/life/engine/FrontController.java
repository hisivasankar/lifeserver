package com.theunknowns.life.engine;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.theunknowns.life.api.DonorListing;
import com.theunknowns.life.api.RegisterGCMToken;
import com.theunknowns.life.api.Registration;
import com.theunknowns.life.api.SendPushRequest;
import com.theunknowns.life.api.UpdateUserLocation;
import com.theunknowns.life.core.BaseService;
import com.theunknowns.life.core.CRUD;

/**
 * Servlet implementation class FrontController
 */
@WebServlet("/FrontController")
public class FrontController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private HashMap<String, Object> serviceRegistry = new HashMap<String, Object>();
	private CRUD crud;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FrontController() {
		super();
		System.out.println("Servlet Constructor");
		crud = new CRUD();
		serviceRegistry.put("GET:/LifeServer/API/DonorListing".toLowerCase(), new DonorListing());
		serviceRegistry.put("GET:/LifeServer/API/UpdateUserLocation".toLowerCase(), new UpdateUserLocation());
		serviceRegistry.put("GET:/LifeServer/API/Registration".toLowerCase(), new Registration());
		serviceRegistry.put("GET:/LifeServer/API/RegisterGCMToken".toLowerCase(), new RegisterGCMToken());
		serviceRegistry.put("GET:/LifeServer/API/SendPushRequest".toLowerCase(), new SendPushRequest());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Object ob = this.getServletContext().getAttribute("javax.websocket.server.ServerContainer");

		/*
		 * try { Class x =
		 * Class.forName("org.apache.tomcat.websocket.server.WsServerContainer"
		 * ); WsServerContainer ws = (WsServerContainer) x.newInstance(); }
		 * catch (InstantiationException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } catch (Exception e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); }
		 */
		handleRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleRequest(request, response);
	}

	private void handleRequest(HttpServletRequest req, HttpServletResponse res) {
		String requestURI = req.getRequestURI();
		String requestMethod = req.getMethod();
		JSONObject responseData = null;
		String requestAPI = (requestMethod + ":" + requestURI).toLowerCase();
		PrintWriter out;
		try {
			out = res.getWriter();
			System.out.println("Incoming Request : " + requestAPI);
			if (serviceRegistry.containsKey(requestAPI)) {
				BaseService service = (BaseService) serviceRegistry.get(requestAPI);
				System.out.println("\tInvoking Service : " + service.getServiceName());
				service.execute(req, res, crud);
			} else {
				System.out.println("\tService not found");
				responseData = new JSONObject();
				responseData.put("msg", "error");
				responseData.put("msg_desc", "Invalid API request");
				out.println(responseData);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

		}

	}

}
