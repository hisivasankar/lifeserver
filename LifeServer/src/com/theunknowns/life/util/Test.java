package com.theunknowns.life.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.theunknowns.life.core.CRUD;

public class Test {
	public void DumpFileToDB() {
		CRUD c = new CRUD();
		File input = new File("C:/Users/i308944/Desktop/G-Learning/Donors.tsv");
		HashMap<String, Integer> bloodMap = new HashMap<String, Integer>();
		bloodMap.put("A+ve".toLowerCase(), 0);
		bloodMap.put("B+ve".toLowerCase(), 1);
		bloodMap.put("O+ve".toLowerCase(), 2);
		bloodMap.put("AB+ve".toLowerCase(), 3);

		bloodMap.put("A-ve".toLowerCase(), 4);
		bloodMap.put("B-ve".toLowerCase(), 5);
		bloodMap.put("O-ve".toLowerCase(), 6);
		bloodMap.put("AB-ve".toLowerCase(), 7);

		bloodMap.put("A1+ve".toLowerCase(), 8);
		bloodMap.put("A1-ve".toLowerCase(), 9);

		bloodMap.put("A1B+ve".toLowerCase(), 10);
		bloodMap.put("A1B-ve".toLowerCase(), 11);

		try {
			FileReader reader = new FileReader(input);
			BufferedReader br = new BufferedReader(reader);
			String line = "";
			while ((line = br.readLine()) != null) {
				String[] data = line.split("\t");
				String id = data[0];
				String name = data[1];
				String email = data[2];
				String mobile = data[3];
				String blood = data[5];
				int bloodId = bloodMap.get(blood.toLowerCase());
				String qryFormat = "INSERT INTO `life`.`donors` (`donor_name`, `donor_mobile`, `donor_email`, `blood_group`, `blood_group_id`) VALUES (\"%s\", '%s', '%s', '%s', '%d');";
				String qry = String.format(qryFormat, name, mobile, email, blood, bloodId);
				boolean result = c.executeQuery(qry);
				if (!result) {
					System.out.println(id);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void updateLocationDetails() throws IOException {
		File locationFile = new File("C:/Users/i308944/Desktop/G-Learning/LatLong.tsv");
		FileReader reader = null;
		BufferedReader br = null;
		try {
			ArrayList<String> donor_ids = new ArrayList<String>();
			String qry = "select donor_id from donors";
			CRUD db = new CRUD();
			ResultSet rs = db.getRecords(qry);
			while (rs.next()) {
				donor_ids.add(rs.getString("donor_id"));
			}
			reader = new FileReader(locationFile);
			br = new BufferedReader(reader);
			String line = "";
			int i = 0;
			while ((line = br.readLine()) != null) {
				String[] data = line.split("\t");
				String lat = data[1];
				String lon = data[3];
				qry = "update donors set latitude=" + lat + ", longitude=" + lon + ", heartbeat='" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "' where donor_id=" + donor_ids.get(i);
				if (db.executeQuery(qry)) {
					System.out.println(i + ". Success");
				} else {
					System.out.println(i + ". Failed");
				}
				i += 1;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (br != null) {
				br.close();
			}
			if (reader != null) {
				reader.close();
			}
		}
	}

	public static void main(String argsp[]) {
		try {
			// new Test().DumpFileToDB();
			new Test().updateLocationDetails();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
