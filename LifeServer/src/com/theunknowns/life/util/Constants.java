package com.theunknowns.life.util;

import java.util.HashMap;

public class Constants {

	public final static String DATA_MSG = "data";
	public final static String NOTIFICATION = "notification";
	public final static String REGISTRATION_IDS = "registration_ids";
	public final static String TO = "to";

	public final static String FAILURE = "failure";
	public final static String CANONICAL_IDS = "canonical_ids";
	public final static String SUCCESS = "success";
	public final static String ERROR = "error";
	public final static String MESSAGE_ID = "message_id";
	public final static String REGISTRATION_ID = "registration_id";
	public final static String NOT_REGISTERED = "NotRegistered";
	public final static String INVALID_REGISTERED = "InvalidRegistration";
	public final static String RESULTS = "results";

	public final static String DONOR_NAME = "donor_name";
	public final static String DONOR_MOBILE = "donor_mobile";
	public final static String DONOR_EMAIL = "donor_email";
	public final static String DONOR_BLOOD_GROUP = "donor_blood_group";
	public final static String DONOR_PASSWORD = "donor_password";
	public final static String DONOR_CITY = "donor_city";
	public final static String REQUESTED_BLOOD_GROUP = "requested_blood_group";
	public final static String LATITUDE = "latitude";
	public final static String LONGITUDE = "longitude";

	public static int getBloodGroupID(String bloodgroup) {
		HashMap<String, Integer> bloodMap = new HashMap<String, Integer>();
		bloodMap.put("A+ve".toLowerCase(), 0);
		bloodMap.put("B+ve".toLowerCase(), 1);
		bloodMap.put("O+ve".toLowerCase(), 2);
		bloodMap.put("AB+ve".toLowerCase(), 3);

		bloodMap.put("A-ve".toLowerCase(), 4);
		bloodMap.put("B-ve".toLowerCase(), 5);
		bloodMap.put("O-ve".toLowerCase(), 6);
		bloodMap.put("AB-ve".toLowerCase(), 7);

		bloodMap.put("A1+ve".toLowerCase(), 8);
		bloodMap.put("A1-ve".toLowerCase(), 9);

		bloodMap.put("A1B+ve".toLowerCase(), 10);
		bloodMap.put("A1B-ve".toLowerCase(), 11);
		return bloodMap.get(bloodgroup.toLowerCase());
	}
}
