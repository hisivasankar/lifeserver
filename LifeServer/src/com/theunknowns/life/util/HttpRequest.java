package com.theunknowns.life.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class HttpRequest {
	public final static String CONTENT_TYPE = "Content-Type";
	public final static String CONTENT_TYPE_JSON = "application/json";
	public final static String CONTENT_TYPE_URLENCODE = "application/x-www-form-urlencoded";

	public final static String AUTHORIZATION = "Authorization";

	private int responseCode;
	private String responseBody;
	private HashMap<String, String> headers;

	public HttpRequest() {
		responseBody = "";
		responseCode = 0;
		headers = new HashMap<String, String>();
	}

	public String getResponse() {
		return responseBody;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void addHeader(String key, String value) {
		headers.put(key, value);
	}

	public void doPost(String url, String requestBody) {
		InputStream input = null;
		OutputStream output = null;
		try {

			URL webAPI = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) webAPI.openConnection();

			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setFixedLengthStreamingMode(requestBody.getBytes().length);
			conn.setRequestMethod("POST");
			for (String key : headers.keySet()) {
				conn.setRequestProperty(key, headers.get(key));
			}
			output = conn.getOutputStream();
			output.write(requestBody.getBytes());

			responseCode = conn.getResponseCode();

			if (responseCode == 200) {
				input = conn.getInputStream();
			} else {
				input = conn.getErrorStream();
			}
			responseBody = readFromStream(input);
			conn.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private String readFromStream(InputStream stream) {
		if (stream == null) {
			return null;
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		StringBuilder data = new StringBuilder();
		String line = "";
		try {
			while ((line = reader.readLine()) != null) {
				data.append(line);
			}
			return data.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
