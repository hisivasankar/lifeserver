package com.theunknowns.life.api;

import java.io.PrintWriter;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.theunknowns.life.core.BaseService;
import com.theunknowns.life.core.CRUD;
import com.theunknowns.life.util.Constants;

public class Registration implements BaseService {
	private final static String SERVICENAME = "registration";

	private final static String ISLOGIN = "islogin";
	private final static String DONOR_NAME = "donor_name";
	private final static String DONOR_MOBILE = "donor_mobile";
	private final static String DONOR_EMAIL = "donor_email";
	private final static String DONOR_BLOOD_GROUP = "donor_blood_group";
	private final static String DONOR_PASSWORD = "donor_password";
	private final static String DONOR_CITY = "donor_city";

	private PrintWriter out;

	@Override
	public String getServiceName() {
		// TODO Auto-generated method stub
		return SERVICENAME;
	}

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response, CRUD crud) {
		try {
			out = response.getWriter();
			String isLogin = request.getParameter(ISLOGIN);
			if (isLogin != null && !isLogin.isEmpty()) {
				String mobile = request.getParameter(DONOR_MOBILE);
				String password = request.getParameter(DONOR_PASSWORD);
				String bloodgroup = request.getParameter(DONOR_BLOOD_GROUP);
				if (isValidParamters(mobile, password, bloodgroup)) {
					String qry = "select donor_name, donor_email, donor_blood_group, donor_city  from donors where donor_mobile='" + mobile + "' and donor_password='" + password + "' and donor_blood_group='" + bloodgroup + "'";
					ResultSet rs = crud.getRecords(qry);
					if (rs.next()) {
						JSONObject donor_info = new JSONObject();
						donor_info.put("msg", "Success");
						donor_info.put(DONOR_MOBILE, mobile);
						donor_info.put(DONOR_PASSWORD, password);
						donor_info.put(DONOR_NAME, rs.getString(DONOR_NAME));
						donor_info.put(DONOR_EMAIL, rs.getString(DONOR_EMAIL));
						donor_info.put(DONOR_BLOOD_GROUP, rs.getString(DONOR_BLOOD_GROUP));
						donor_info.put(DONOR_CITY, rs.getString(DONOR_CITY));
						out.print(donor_info.toString());
						System.out.println("\t\tTo Client : " + donor_info.toString());
					} else {
						JSONObject res = createMessgae("error", "login failed");
						out.print(res.toString());
						System.out.println("\t\tTo Client : " + res.toString());
					}
				} else {
					JSONObject res = createMessgae("error", "parameters missing.");
					out.print(res.toString());
					System.out.println("\t\tTo Client : " + res.toString());
				}
			} else {
				String name = request.getParameter(DONOR_NAME);
				String mobile = request.getParameter(DONOR_MOBILE);
				String password = request.getParameter(DONOR_PASSWORD);
				String email = request.getParameter(DONOR_EMAIL);
				String bloodgroup = request.getParameter(DONOR_BLOOD_GROUP);
				String city = request.getParameter(DONOR_CITY);
				if (isValidParamters(name, mobile, password, email, bloodgroup, city)) {
					int bloodId = Constants.getBloodGroupID(bloodgroup);
					String qry = "select donor_id from donors where donor_mobile='" + mobile + "'";
					ResultSet rs = crud.getRecords(qry);
					if (rs.next()) {
						qry = "UPDATE `life`.`donors` SET `donor_name`='" + name + "', `donor_email`='" + email + "', `donor_blood_group`='" + bloodgroup + "', `blood_group_id`='" + bloodId
								+ "', `donor_city`='" + city + "', `donor_password`='" + password + "' WHERE `donor_mobile`='" + mobile + "';";
						boolean result = crud.executeQuery(qry);
						if (result) {
							JSONObject donor_info = new JSONObject();
							donor_info.put("msg", "Success");
							donor_info.put("msg_desc", "User updated successfully");
							donor_info.put(DONOR_MOBILE, mobile);
							donor_info.put(DONOR_PASSWORD, password);
							donor_info.put(DONOR_NAME, name);
							donor_info.put(DONOR_EMAIL, email);
							donor_info.put(DONOR_BLOOD_GROUP, bloodgroup);
							donor_info.put(DONOR_CITY, city);
							out.print(donor_info.toString());
							System.out.println("\t\tTo Client : " + donor_info.toString());
							System.out.println("\t\t Service Called Success");
						} else {
							JSONObject res = createMessgae("error", "failed to update user information");					
							System.out.println("\t\tTo Client : " + res.toString());
							System.out.println("\t\t Service Called Failed");
						}
					} else {
						qry = "INSERT INTO `life`.`donors` (`donor_name`, `donor_mobile`, `donor_email`, `donor_blood_group`, `blood_group_id`, `donor_city`, `donor_password`, `heartbeat`) VALUES " + "('" + name
								+ "', '" + mobile + "', '" + email + "', '" + bloodgroup + "'," + bloodId + ", '" + city + "', '" + password + "','" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "')";
						boolean result = crud.executeQuery(qry);
						if (result) {
							JSONObject donor_info = new JSONObject();
							donor_info.put("msg", "Success");
							donor_info.put("msg_desc", "User created successfully");
							donor_info.put(DONOR_MOBILE, mobile);
							donor_info.put(DONOR_PASSWORD, password);
							donor_info.put(DONOR_NAME, name);
							donor_info.put(DONOR_EMAIL, email);
							donor_info.put(DONOR_BLOOD_GROUP, bloodgroup);
							donor_info.put(DONOR_CITY, city);
							out.print(donor_info.toString());
							System.out.println("\t\tTo Client : " + donor_info.toString());
							System.out.println("\t\t Service Called Success");
						} else {
							String res = createMessgae("error", "failed to create user").toString();
							System.out.println("\t\tTo Client : " + res.toString());
							System.out.println("\t\t Service Called Failed");
						}
					}
				} else {
					JSONObject res = new JSONObject();
					res.put("msg", "error");
					res.put("msg_desc", "paramters missing.");
					out.print(res.toString());
					System.out.println("\t\tTo Client : " + res.toString());
					System.out.println("\t\t Service Called Failed");
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private boolean isValidParamters(String... params) {
		for (int i = 0; i < params.length; i++) {
			if (params[i] == null || params[i].trim().isEmpty()) {
				return false;
			}
		}
		return true;
	}

	private JSONObject createMessgae(String tag, String message) {
		JSONObject temp = new JSONObject();
		temp.put("msg", tag);
		temp.put("msg_desc", message);
		return temp;
	}
}
