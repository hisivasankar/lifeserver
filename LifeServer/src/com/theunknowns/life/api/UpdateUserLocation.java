package com.theunknowns.life.api;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.theunknowns.life.core.BaseService;
import com.theunknowns.life.core.CRUD;

public class UpdateUserLocation implements BaseService {
	private final static String SERVICENAME = "UpdateUserLocation";

	private final static String DONOR_MOBILE = "donor_mobile";
	private final static String LATITUDE = "latitude";
	private final static String LONGITUDE = "longitude";

	private PrintWriter out;
	@Override
	public String getServiceName() {
		return SERVICENAME;
	}

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response, CRUD crud) {
		try {
			out = response.getWriter();
			
			JSONObject result = new JSONObject();
			
			String mobile = request.getParameter(DONOR_MOBILE);
			String latitude = request.getParameter(LATITUDE);
			String longitude = request.getParameter(LONGITUDE);
			
			if(mobile != null && latitude != null && longitude != null &&
					!mobile.isEmpty() && !latitude.isEmpty() && !longitude.isEmpty()) {
				System.out.println("Service call success");
				System.out.println("\t\tFrom Client : " + mobile);
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date now = new Date();
				String qry = "UPDATE `life`.`donors` SET `latitude`='" + latitude + "', `longitude`='" + longitude + "', `heartbeat`= '" + df.format(now) +"' WHERE `donor_mobile`='" + mobile +"'";
				boolean dbOperation = crud.executeQuery(qry);
				if(dbOperation) {
					result.put("msg", "true");
					result.put("msg_desc", "User location updated successfully");					
				} else {
					result.put("msg", "false");
					result.put("msg_desc", "Update failed");
				}
				out.print(result);
				System.out.println("\t\tTo Client : " + result.toString());
				System.out.println("\t\t Service Called Success");
			} else {
				result.put("msg", "failed");
				result.put("msg_desc", "Parameter missing");
				out.print(result);
				System.out.println("\t\tTo Client : " + result.toString());
				System.out.println("\t\t Service Called Failed");
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

}
