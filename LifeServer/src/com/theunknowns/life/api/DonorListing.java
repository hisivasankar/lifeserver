package com.theunknowns.life.api;

import java.io.PrintWriter;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.theunknowns.life.core.BaseService;
import com.theunknowns.life.core.CRUD;
import com.theunknowns.life.util.Helper;

public class DonorListing implements BaseService{
	private final static String SERVICENAME = "DonorListing";
	
	private PrintWriter out;
	private final static String BLOODGROUP = "bloodGroup";
	private final static String ISGEOLOCATIONENABLED = "geolocation";	
	private final static String DONOR_NAME = "donor_name";
	private final static String DONOR_MOBILE = "donor_mobile";
	private final static String DONOR_EMAIL = "donor_email";
	private final static String DONOR_BLOOD_GROUP = "donor_blood_group";
	private final static String DONOR_CITY = "donor_city";

	private final static String LATITUDE = "latitude";
	private final static String LONGITUDE = "longitude";
	private final static String LASTUPDATEDON = "heartbeat";
	
			
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response, CRUD crud) {
		try {
			out = response.getWriter();
			JSONArray donor_list = new JSONArray();	
			String donorMobile = request.getParameter(DONOR_MOBILE);
			String bloodGroup = request.getParameter(BLOODGROUP);
			String donorcity = request.getParameter(DONOR_CITY);
			String isGeoLocationRequired =  request.getParameter(ISGEOLOCATIONENABLED);
			if(Helper.isValidParamters(bloodGroup, donorMobile) && donorcity != null) {	
				System.out.println("\t\tFrom Client : " + donorMobile);
				if(!Helper.isValidParamters(isGeoLocationRequired)) {
					String qry = "SELECT * FROM life.donors WHERE donor_blood_group='" + bloodGroup + "' and donor_mobile !='" + donorMobile +  "' and lower(donor_city) like lower('%" + donorcity.trim() + "%')  order by donor_name asc";					 			
					ResultSet rs = crud.getRecords(qry);
					if(rs != null) {
						while(rs.next()) {
							JSONObject donor = new JSONObject();
							donor.put(DONOR_NAME, rs.getString(DONOR_NAME));
							donor.put(DONOR_MOBILE, rs.getString(DONOR_MOBILE));
							donor.put(DONOR_EMAIL, rs.getString(DONOR_EMAIL));
							donor.put(DONOR_BLOOD_GROUP, rs.getString(DONOR_BLOOD_GROUP));
							donor.put(DONOR_CITY, rs.getString(DONOR_CITY) + "(*)");
							donor_list.add(donor);			
						}
					}				
				} else {
					String qry = "SELECT * FROM life.donors WHERE donor_blood_group='" + bloodGroup + "' and donor_mobile !='" + donorMobile + "' order by donor_name asc";	
					ResultSet rs = crud.getRecords(qry);
					while(rs.next()) {
						JSONObject donor = new JSONObject();
						donor.put(DONOR_NAME, rs.getString(DONOR_NAME));
						donor.put(DONOR_MOBILE, rs.getString(DONOR_MOBILE));
						donor.put(DONOR_EMAIL, rs.getString(DONOR_EMAIL));
						donor.put(DONOR_BLOOD_GROUP, rs.getString(DONOR_BLOOD_GROUP));
						donor.put(LATITUDE, rs.getDouble(LATITUDE));
						donor.put(LONGITUDE, rs.getDouble(LONGITUDE));									
						donor.put(LASTUPDATEDON, rs.getString(LASTUPDATEDON));
						donor.put(DONOR_CITY, rs.getString(DONOR_CITY) + "(*)");
						donor_list.add(donor);			
					}
				}			
				JSONObject data = new JSONObject();
				data.put("msg", "success");
				data.put("donors_list", donor_list);
				out.print(data);
				System.out.println("\t\tTo Client : " + data.toString());
				System.out.println("\t\t Service Called Success");							
			} else {
				JSONObject data = new JSONObject();
				data.put("msg", "error");
				data.put("msg_desc", "Parameters missing");
				out.print(data);
				System.out.println("\t\tTo Client : " + data.toString());
				System.out.println("\t\t Service Called Failed");							
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
	}


	@Override
	public String getServiceName() {
		return SERVICENAME;	
	}

}
