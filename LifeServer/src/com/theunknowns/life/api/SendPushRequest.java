package com.theunknowns.life.api;

import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.theunknowns.life.core.BaseService;
import com.theunknowns.life.core.CRUD;
import com.theunknowns.life.util.AppConfig;
import com.theunknowns.life.util.Constants;
import com.theunknowns.life.util.Helper;
import com.theunknowns.life.util.HttpRequest;

public class SendPushRequest implements BaseService {
	private final static String SERVICENAME = "SendPushRequest";

	private final static String DONOR_MOBILE = "donor_mobile";
	private final static String ISBRODADCAST = "is_broadcast";
	private final static String BROADCAST_BLOODGROUP = "broadcast_bloodgroup";
	private final static String P2PID = "p2p_id";
	
	private final static String DONOR_GCM_TOKEN = "donor_gcm_token";
	
	private static String from_donor_gcm_token, from_donor_name, from_donor_mobile, from_donor_email, requested_blood_group, from_donor_city;
	private static double from_donor_latitude, from_donor_longitude;
	
	private CRUD crud;

	private PrintWriter out;
	@Override
	public String getServiceName() {
		return SERVICENAME;
	}

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response, CRUD crud) {
		try {
			this.crud = crud;
			out = response.getWriter();			
			JSONObject result = new JSONObject();
			
			String mobile = request.getParameter(DONOR_MOBILE);
			String isbroadcast = request.getParameter(ISBRODADCAST);
			String broadcast_bloodgroup = request.getParameter(BROADCAST_BLOODGROUP);
			String p2pid = request.getParameter(P2PID);
							
			if(Helper.isValidParamters(mobile)) {				
				if(Helper.isValidParamters(isbroadcast, broadcast_bloodgroup)) {
					requested_blood_group = broadcast_bloodgroup;
					if(isValidUser(mobile, crud)) {
						String qry = "select donor_gcm_token from donors where donor_blood_group='" + broadcast_bloodgroup + "' and donor_gcm_token != '' and donor_mobile !='" + mobile + "'";
						ResultSet rs = crud.getRecords(qry);
						JSONArray list = new JSONArray();
						while(rs.next()) {
							list.add(rs.getString(DONOR_GCM_TOKEN));
						}						
						if(list.size() == 0) {
							System.out.println("\t\tNo user has subscribed to GCM");
							sendErrorMessage("error", "No user has subscribed to Push Notification", out);
							return;
						}
						JSONObject dataToSend = new JSONObject();						
						JSONObject payload = new JSONObject();
						payload.put(Constants.DONOR_NAME,from_donor_name);
						payload.put(Constants.DONOR_MOBILE, from_donor_mobile);
						payload.put(Constants.DONOR_EMAIL, from_donor_email);
						payload.put(Constants.REQUESTED_BLOOD_GROUP, requested_blood_group);
						payload.put(Constants.DONOR_CITY, from_donor_city);
						if(from_donor_latitude != 0 && from_donor_latitude != 0) {
							payload.put(Constants.LATITUDE, from_donor_latitude);
							payload.put(Constants.LONGITUDE, from_donor_longitude);
						}
						dataToSend.put(Constants.REGISTRATION_IDS, list);
						dataToSend.put(Constants.DATA_MSG, payload);
						System.out.println("\t\tData To GCM Server : " + dataToSend);
						HttpRequest httpRequest = new HttpRequest();
						httpRequest.addHeader(HttpRequest.AUTHORIZATION, AppConfig.GCM_SERVER_KEY);
						httpRequest.addHeader(HttpRequest.CONTENT_TYPE, HttpRequest.CONTENT_TYPE_JSON);
						httpRequest.doPost(AppConfig.GOOGLE_GCM_HTTP_SERVER, dataToSend.toString());
						int responseCode = httpRequest.getResponseCode();
						String respString = httpRequest.getResponse();
						System.out.println("\t\tData from GCM : " + responseCode);
						System.out.println("\t\tData from GCM : " + respString);
						JSONObject dataToClient = processGCMResponse(list, responseCode, respString);
						if(dataToClient != null) {
							System.out.println("\t\tService call success");
							System.out.println("\t\tFrom Client : " + mobile);
							System.out.println("\t\tTo Client : " + dataToClient);
							out.print(dataToClient.toString());
						} else {
							System.out.println("\t\tUnable to parse gcm response.");
							sendErrorMessage("error", "Failed due to unknown reason", out);
						}
						
					} else {
						sendErrorMessage("error", "Invalid user.", out);
						return;
					}									
				} else if(Helper.isValidParamters(p2pid)) {
					// Peer to peer push notification
				} else {
					sendErrorMessage("error", "parameters invalid", out);
				}				
			} else {
				System.out.println("\t\tParamters missing.");
				sendErrorMessage("error", "Failed due to unknown reason", out);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	private void sendErrorMessage(String msg, String msg_desc, PrintWriter out) {
		JSONObject result = new JSONObject();	
		result.put("msg", msg);
		result.put("msg_desc", msg_desc);
		out.print(result);
		System.out.println("\t\tTo Client : " + result.toString());
		System.out.println("\t\t Service Called Failed");
	}
	private Object readFromString(String data) {
		JSONParser parser = new JSONParser();
		try {
			Object object = parser.parse(data);
			return object;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	private boolean isValidUser(String mobile, CRUD c) throws SQLException {
		String qry = "select donor_name, donor_mobile, donor_email, donor_blood_group, donor_city, latitude, longitude, donor_gcm_token from donors where donor_mobile='" + mobile + "'";
		ResultSet rs = c.getRecords(qry);
		if(rs != null && rs.next()) {		
			from_donor_gcm_token = rs.getString(DONOR_GCM_TOKEN);
			from_donor_name = rs.getString(Constants.DONOR_NAME);
			from_donor_email = rs.getString(Constants.DONOR_EMAIL);
			from_donor_mobile = rs.getString(Constants.DONOR_MOBILE);			
			from_donor_city = rs.getString(Constants.DONOR_CITY) + "(*)";
			from_donor_latitude = rs.getDouble(Constants.LATITUDE);
			from_donor_longitude = rs.getDouble(Constants.LONGITUDE);
			return true;
		} else {
			return false;
		}
	}
	private JSONObject processGCMResponse(JSONArray donor_list, int responseCode, String responseString) {
		try {			
			if(responseCode == 401 || responseCode == 400) {
				System.out.println("Authentication Failed. Please check api key");
				JSONObject data = new JSONObject();
				data.put("msg", "error");				
				data.put("msg_desc", "Please contact Administration for Push Notification");
				return data;
			} else if(responseCode == 200) {
				JSONParser parser = new JSONParser();
				JSONObject obj = (JSONObject) parser.parse(responseString);
				long failure = (Long) obj.get(Constants.FAILURE);
				long canonical_id = (Long) obj.get(Constants.CANONICAL_IDS);
				long success = (Long) obj.get(Constants.SUCCESS);
				if( failure == 0 && canonical_id == 0) {
					JSONObject data = new JSONObject();
					data.put("msg", "success");
					data.put("success", success);
					data.put("msg_desc", "Broadcast Success.");
					return data;
				} else {
					JSONArray results = (JSONArray) obj.get(Constants.RESULTS);
					for(int i=0; i < results.size(); i++) {
						JSONObject item = (JSONObject) results.get(i);
						String message_id = (String) item.get(Constants.MESSAGE_ID);
						String new_reg_id = (String) item.get(Constants.REGISTRATION_ID);
						String error = (String) item.get(Constants.ERROR);
						if(message_id != null && new_reg_id != null) {
							updateGCMToken((String) donor_list.get(i), new_reg_id);
						} else if(error != null && (error.equalsIgnoreCase(Constants.NOT_REGISTERED) || error.equalsIgnoreCase(Constants.INVALID_REGISTERED))) {
							updateGCMToken((String) donor_list.get(i), "");
						}
					}
					if(success > 0) {
						JSONObject data = new JSONObject();
						data.put("msg", "success");
						data.put("success", success);
						data.put("msg_desc", "Broadcast Success.");
						return data;
					} else {
						JSONObject data = new JSONObject();
						data.put("msg", "error");
						data.put("msg_desc", "No user has subscribed to Push notification");
						return data;
					}
				}
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}		
		return null;
	}
	private String createNotificationMessage(String title, String text) {
		return null;
	}
	private void updateGCMToken(String old_Token, String new_Token) throws SQLException {
		String qry = "";
		if(new_Token.isEmpty()) {
			qry = "update donors set donor_gcm_token = NULL where donor_gcm_token='" + old_Token + "'";	
		} else {
			qry = "update donors set donor_gcm_token = '" + new_Token + "' where donor_gcm_token='" + old_Token + "'";
		}
		crud.executeQuery(qry);
	}
	

}
