package com.theunknowns.life.api;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.theunknowns.life.core.BaseService;
import com.theunknowns.life.core.CRUD;
import com.theunknowns.life.util.Helper;

public class RegisterGCMToken implements BaseService{
	private final static String SERVICENAME = "RegisterGCMToken";
	
	private final static String DONOR_MOBILE = "donor_mobile";
	private final static String DONOR_REGISTRATION_TOKEN = "donor_gcm_token";
	
	PrintWriter out;
	@Override
	public String getServiceName() {
		// TODO Auto-generated method stub
		return SERVICENAME;
	}

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response, CRUD crud) {
		try {			
			out = response.getWriter();
			String donormobile = request.getParameter(DONOR_MOBILE);
			String donorgcmtoken = request.getParameter(DONOR_REGISTRATION_TOKEN);
			JSONObject resp;
			if(Helper.isValidParamters(donormobile, donorgcmtoken)) {
				System.out.println("Service call success");
				System.out.println("\t\tFrom Client : " + donormobile);
				String qry = "UPDATE `life`.`donors` SET `donor_gcm_token`='" + donorgcmtoken + "' WHERE `donor_mobile`='" + donormobile + "';";
				boolean result = crud.executeQuery(qry);
				if(result) {
					resp = new JSONObject();
					resp.put("msg", "Success");
					resp.put("msg_desc", "Token updated successfully");
					out.print(resp.toString());
					System.out.println("\t\tTo Client : " + resp.toString());
					System.out.println("\t\t Service Called Success");
				} else {
					resp = new JSONObject();
					resp.put("msg", "error");
					resp.put("msg_desc", "failed due to unkown reason");
					out.print(resp.toString());
					System.out.println("\t\tTo Client : " + resp.toString());
					System.out.println("\t\t Service Called Success");
				}
			} else {
				resp = new JSONObject();
				resp.put("msg", "error");
				resp.put("msg_desc", "parameters missing.");
				out.print(resp.toString());
				System.out.println("\t\tTo Client : " + resp.toString());
				System.out.println("\t\t Service Called Success");
			}
		}catch(Exception ex) {
			JSONObject resp = new JSONObject();
			resp.put("msg", "error");
			resp.put("msg_desc", "unknown exception happened.");
			out.print(resp);
			System.out.println("\t\tTo Client : " + resp.toString());
			System.out.println("\t\t Service Called Success");
			ex.printStackTrace();
		}
		
	}

}
