package com.theunknowns.life.core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class CRUD implements BaseDBOperations {
	String username = "root";
	String password = "root123";
	String dbName = "life";
	String className = "com.mysql.jdbc.Driver";
	String connStirng = "jdbc:mysql://localhost:3306/life";

	private Connection conn;
	private PreparedStatement pstmt;

	public CRUD() {
		try {
			Class.forName(className);
			conn = DriverManager.getConnection(connStirng, username, password);
			System.out.println("Database connected");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public ResultSet getRecords(String query) {
		ResultSet rs = null;
		try {
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return rs;
	}

	public boolean executeQuery(String query) throws SQLException {
		try {
			pstmt = conn.prepareStatement(query);
			int count = pstmt.executeUpdate();
			if (count > 0) {
				return true;
			} else {
				return false;
			}
		} catch (MySQLIntegrityConstraintViolationException ex) {
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}

	}

}
