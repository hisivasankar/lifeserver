package com.theunknowns.life.core;

import java.sql.ResultSet;

public interface BaseDBOperations {
	ResultSet getRecords(String query);
}
