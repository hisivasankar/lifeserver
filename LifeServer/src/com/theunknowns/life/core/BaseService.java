package com.theunknowns.life.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface BaseService {

	String getServiceName();

	void execute(HttpServletRequest request, HttpServletResponse response, CRUD crud);

}
